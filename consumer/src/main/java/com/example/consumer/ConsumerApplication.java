package com.example.consumer;

import com.example.consumer.dto.MysqlDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = "com.example.consumer")
@RestController
@EnableDiscoveryClient
public class ConsumerApplication {

    @Value("${name}")
    private String name;

    @Value("${money}")
    private Integer money;

    @Autowired
    private MysqlDto mysqlDto;


    @GetMapping("/config")
    public String get(){
        return mysqlDto.toString();
    }

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/getApp")
    public String getApp(){
        String str = restTemplate.getForObject("http://producer-1/app",String.class);
        System.out.println("name="+name+",money="+money);
        return str;
    }
}
