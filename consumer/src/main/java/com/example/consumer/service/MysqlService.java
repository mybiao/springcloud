package com.example.consumer.service;

import com.example.consumer.dto.MysqlDto;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class MysqlService {

    private final MysqlDto mysqlDto;

    public MysqlService(MysqlDto mysqlDto) {
        this.mysqlDto = mysqlDto;
    }

    @PostConstruct
    public void run(){
        System.out.println(mysqlDto);
        System.out.println("hello example");
    }
}
